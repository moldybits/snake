# snake

tsss

## todo
* [x] integer scaling of gamescreen
* [x] align fruits on grid
* [x] when touching fruit:
  * [x] remove fruit
  * [x] decrease cooldown-max
  * [x] increase tail
* [x] wrap around edge of screen
* [ ] objects
  * [ ] teleports
  * [ ] obstacle
  * [ ] moving fruit
  * [ ] object that slows you down
  * [ ] object that reduces your length
* [ ] snake
  * [ ] button press to stick out tongue
* [ ] images
  * [x] head (placeholder)
  * [x] body (placeholder)
  * [x] tail (placeholder)
  * [x] fruit (placeholder)
  * [ ] obstacle?
  * [ ] title logo
* [ ] sound
  * [ ] SFX when moving
  * [ ] SFX when eating/lengthening
  * [ ] SFX when crashing
  * [ ] SFX in menu?
  * [ ] MELODY for title
  * [ ] MELODY for game start (like in pacman)
  * [ ] MELODY for game over
* [ ] interface
  * [ ] font
  * [ ] title menu
  * [ ] pause menu

advanced features
* [ ] replay
* [ ] multiplayer single machine
* [ ] multiplayer over internet
