(asdf:defsystem :snake
  :description "This is just a simple snake game."
  :author "moldybits,
decent-username <decent-username@not-a-real-email.xyz>"
  :version "0.0.1"
  :depends-on (lispbuilder-sdl)
  :serial t
  :pathname "src/"
  :components
  ((:file "packages")
   (:file "snake")))
