(in-package :snake)

(defparameter *fps* 60)
(defparameter *initial-cooldown* 15)
(defparameter *grid-width* 32)
(defparameter *grid-height* 32)
(defparameter *board-width* 16)
(defparameter *board-height* 16)
(defparameter *screen-width* (* *grid-width* *board-width*))
(defparameter *screen-height* (* *grid-height* *board-height*))
(defparameter *scale-x* 1)
(defparameter *scale-y* 1)

(defparameter *theme* `((object     . ,sdl:*white*)
                        (snake-head . ,sdl:*yellow*)
                        (snake-part . ,sdl:*green*)
                        (fruit      . ,sdl:*red*)))

;; initialized by init
(defparameter *screen* nil)          ; sdl must be initialized
(defparameter *snake* nil)
(defparameter *board* nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;; misc
(defun facing->offset (facing)
  (case facing
    (:east (list   1  0))
    (:west (list  -1  0))
    (:south (list  0  1))
    (:north (list  0 -1))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; class definitions
(defclass object ()
  ())

(defclass fruit (object) ())

(defclass snake-part (object)
  ((x :accessor x :initarg :x)
   (y :accessor y :initarg :y)
   (child :accessor child :initarg :child :initform nil)))

(defclass snake-head (snake-part)
  ((cooldown
    :accessor cooldown
    :initarg :cooldown
    :initform *initial-cooldown*)       ; can you refer to cooldown-max?
   (cooldown-max                        ; maybe -init
    :accessor cooldown-max
    :initarg :cooldown-max
    :initform *initial-cooldown*)
   (facing
    :accessor facing
    :initarg :facing)))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;; methods

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; drawing methods
(defmethod draw ((o object))
  (draw-at o (x o) (y o)))

(defmethod draw-at ((o object) x y)
  (sdl:draw-box (sdl:rectangle :x (* *grid-width* x) :y (* *grid-height* y)
                               :w *grid-width*       :h *grid-height*)
                :color (cdr (assoc (type-of o) *theme*))))

(defmethod draw-at ((o snake-part) x y)
  (when (child o)
    (draw (child o)))
  (call-next-method o x y))

;;;;;;;;;;;;;;;;;;;;
;;;; moving methods

(defmethod move-to ((o object) x y)
  (when (child o)
    (move-to (child o) (x o) (y o)))
  (setf (x o) x
        (y o) y))

(defmethod advance ((o snake-head))
  (let* ((new-pos (mapcar #'+ (facing->offset (facing o))
                          (list (x o) (y o))))
         (new-pos (mapcar #'mod new-pos
                          (list *board-width* *board-height*))))
    (apply #'move-to o new-pos)))

;;;;;;;;;;;;;;;;;;;;
;;;; logic methods
(defmethod think! ((o object)))

(defmethod think! ((o snake-head))
  (decf (cooldown o))
  (when (<= (cooldown o) 0)
    (setf (cooldown o) (cooldown-max o))
      
    (advance o)

    ;; check tail collision
    (do ((c (child o) (child c)))
        ((null c))
      (when (equal (list (x o) (y o))
                   (list (x c) (y c)))
        (format t "tail(~S) collision!~%" c)))

    ;; check fruit collision
    (when (aref *board* (x o) (y o))
      (setf (aref *board* (x o) (y o)) nil)
      (setf (aref *board* (random *board-width*) (random *board-height*))
            (make-instance 'fruit))
      (setf (cooldown-max o) (max 1 (1- (cooldown-max o))))
      (lengthen! o))))

;;;;;;;;;;;;;;;;;;;;
;;;; special methods
(defmethod lengthen! ((o snake-part))
  (if (null (child o))
      (setf (child o) (make-instance 'snake-part :x (x o) :y (y o)))
      (lengthen! (child o))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;;;; main

(defun init ()
  (setf *snake* (make-instance 'snake-head :x 0 :y 0 :facing :south))
  (setf *board* (make-array (list *board-width* *board-height*) :initial-element nil))
  (setf (aref *board* (random *board-width*) (random *board-height*))
        (make-instance 'fruit))
  (setf *screen* (sdl:create-surface *screen-width* *screen-height*))
  (setf sdl:*default-surface* *screen*))

(defun start (&key (init t))
  (sdl:with-init ()
    (sdl:window (* *scale-x* *screen-width*)
                (* *scale-y* *screen-height*)
                :title-caption "tsss")
    (setf (sdl:frame-rate) *fps*)

    (when init
      (init))

    (sdl:with-events ()
      (:quit-event () t)
      (:key-down-event (:key key)
        (when (member (facing *snake*) '(:south :north))
          (case key
            (:sdl-key-right 
             (setf (facing *snake*) :east)
             (setf (cooldown *snake*) 0))
            (:sdl-key-left
             (setf (facing *snake*) :west)
             (setf (cooldown *snake*) 0))))

        (when (member (facing *snake*) '(:west :east))
          (case key
            (:sdl-key-down
             (setf (facing *snake*) :south)
             (setf (cooldown *snake*) 0))
            (:sdl-key-up 
             (setf (facing *snake*) :north)
             (setf (cooldown *snake*) 0))))

        (case key
          (:sdl-key-escape (sdl:push-quit-event))))
      (:mouse-button-down-event ()
                                (let ((x (floor (/ (sdl:mouse-x) *grid-width*)))
                                      (y (floor (/ (sdl:mouse-y) *grid-height*))))
                                  (setf (aref *board* x y)
                                        (make-instance 'fruit))))
      (:idle ()
        ;; think
        (think! *snake*)

        ;; draw
        (sdl:clear-display sdl:*black* :surface *screen*)

        (destructuring-bind (n m) (array-dimensions *board*)
          (loop :for i :from 0 :below n :do
               (loop :for j :from 0 :below m :do
                    (let ((fruit (aref *board* i j)))
                      (when fruit
                        (draw-at fruit i j))))))

        (draw *snake*)

        (let ((temp  (sdl:zoom-surface *scale-x* *scale-y* :surface *screen*)))
          (sdl:blit-surface temp sdl:*default-display*)
          (sdl:free temp))

        (sdl:update-display)))))
